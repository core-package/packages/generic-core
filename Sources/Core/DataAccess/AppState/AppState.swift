//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public class AppState: ObservableObject {
    @Published var theme: Theme
    
    init() {
        self.theme = DefaultTheme()
    }
}
