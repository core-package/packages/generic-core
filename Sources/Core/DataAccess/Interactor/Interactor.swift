//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

public protocol Interactor {
    /* This is a Dummy interactor protocol for the Coordinator.
       The Real Interactor should include the Relative functions required for the App Specifications.
     */
}
