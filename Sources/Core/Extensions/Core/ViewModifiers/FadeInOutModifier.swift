//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public enum AnimationEffect {
    case fadeInOut
    case scaleEffect(CGFloat)
    case rotateEffect(Double)
}

public struct CustomAnimationModifier: ViewModifier {
    let effect: AnimationEffect
    let duration: TimeInterval

    @State private var isAnimating = false

    public func body(content: Content) -> some View {
        content
            .opacity(opacity)
            .scaleEffect(scale)
            .rotationEffect(rotation)
            .animation(Animation.easeInOut(duration: duration).repeatForever(autoreverses: true))
            .onAppear {
                isAnimating = true
            }
    }

    private var opacity: Double {
        switch effect {
        case .fadeInOut:
            return isAnimating ? 0 : 1
        default:
            return 1
        }
    }

    private var scale: CGFloat {
        switch effect {
        case .scaleEffect(let scaleFactor):
            return isAnimating ? scaleFactor : 1
        default:
            return 1
        }
    }

    private var rotation: Angle {
        switch effect {
        case .rotateEffect(let angle):
            return isAnimating ? .degrees(angle) : .degrees(0)
        default:
            return .degrees(0)
        }
    }
}
