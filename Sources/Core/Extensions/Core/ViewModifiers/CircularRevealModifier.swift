//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct CircularRevealModifier: ViewModifier {
    let isRevealed: Bool

    public func body(content: Content) -> some View {
        content
            .mask(Circle().scale(isRevealed ? 1 : 0.0001))
            .animation(.spring(response: 0.5, dampingFraction: 0.5, blendDuration: 0))
    }
}
