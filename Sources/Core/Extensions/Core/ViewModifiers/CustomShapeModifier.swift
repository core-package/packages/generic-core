//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct CustomShapeModifier<S: Shape>: ViewModifier {
    let shape: S

    public func body(content: Content) -> some View {
        content
            .clipShape(shape)
    }
}
