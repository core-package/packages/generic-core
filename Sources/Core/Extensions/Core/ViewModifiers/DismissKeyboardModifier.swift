//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct DismissKeyboardButtonModifier: ViewModifier {
    public func body(content: Content) -> some View {
        content
            .navigationBarItems(trailing: Button(action: {
                UIApplication.shared.dismissKeyboard()
            }) {
                Image(systemName: "keyboard.chevron.compact.down")
            })
    }
}

public extension UIApplication {
    public func dismissKeyboard() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
