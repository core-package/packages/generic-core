//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct GrayscaleModifier: ViewModifier {
    public func body(content: Content) -> some View {
        content
            .colorMultiply(Color.gray)
    }
}
