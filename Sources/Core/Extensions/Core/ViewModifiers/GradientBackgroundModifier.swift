//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct GradientBackgroundModifier: ViewModifier {
    let colors: [Color]

    public func body(content: Content) -> some View {
        content
            .background(LinearGradient(gradient: Gradient(colors: colors),
                                       startPoint: .top, endPoint: .bottom))
    }
}
