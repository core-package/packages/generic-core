//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct ShakeModifier: ViewModifier {
    let times: Int
    let offset: CGFloat

    public func body(content: Content) -> some View {
        content
            .animation(Animation.linear(duration: 0.1).repeatCount(times, autoreverses: true))
            .offset(x: CGFloat(times % 2 == 0 ? 0 : offset))
    }
}

