//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct SingleSideShadow: ViewModifier {
    let color: Color
    let radius: CGFloat
    let x: CGFloat
    let y: CGFloat

    public func body(content: Content) -> some View {
        content
            .overlay(content.shadow(color: color, radius: radius, x: x, y: y))
    }
}
