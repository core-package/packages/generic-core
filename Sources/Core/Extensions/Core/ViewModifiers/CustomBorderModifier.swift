//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct CustomBorderModifier<S: ShapeStyle>: ViewModifier {
    let content: S
    let width: CGFloat

    public func body(content: Content) -> some View {
        content
            .overlay(RoundedRectangle(cornerRadius: 5)
                .stroke(self.content, lineWidth: width))
    }
}
