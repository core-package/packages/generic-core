//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct PulsateModifier: ViewModifier {
    let scale: CGFloat
    let duration: TimeInterval

    @State private var isAnimating = false

    public func body(content: Content) -> some View {
        content
            .scaleEffect(isAnimating ? scale : 1)
            .animation(Animation.easeInOut(duration: duration).repeatForever(autoreverses: true))
            .onAppear {
                isAnimating = true
            }
    }
}
