//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct CustomCornerRadiusModifier: ViewModifier {
    let radius: CGFloat
    let corners: UIRectCorner

    public func body(content: Content) -> some View {
        content
            .clipShape(RoundedCorner(radius: radius, corners: corners))
    }
}

