//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct BackgroundBlurModifier: ViewModifier {
    let radius: CGFloat

    public func body(content: Content) -> some View {
        content
            .background(BlurView(radius: radius))
    }
}


