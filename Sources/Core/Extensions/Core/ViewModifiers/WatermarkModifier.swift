//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct WatermarkModifier: ViewModifier {
    let text: String

    public func body(content: Content) -> some View {
        ZStack {
            content
            Text(text)
                .font(.caption)
                .foregroundColor(Color.gray.opacity(0.5))
                .padding(5)
                .background(Color.white.opacity(0.7))
                .cornerRadius(5)
                .offset(x: 20, y: 20)
        }
    }
}

