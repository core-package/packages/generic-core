//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct CustomFontModifier: ViewModifier {
    let name: String
    let size: CGFloat
    
    public func body(content: Content) -> some View {
        content.font(Font.custom(name, size: size))
    }
}
