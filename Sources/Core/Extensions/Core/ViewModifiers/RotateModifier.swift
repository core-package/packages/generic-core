//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public struct RotateModifier: ViewModifier {
    let degrees: Double

    public func body(content: Content) -> some View {
        content
            .rotationEffect(.degrees(degrees))
    }
}

