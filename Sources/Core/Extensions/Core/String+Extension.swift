//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

public extension String {
    // MARK: TO INT
    var toInt: Int? {
        return Int(self)
    }
    
    /*
     // Example usage:
     let stringValue = "42"
     let intValue = stringValue.toInt
     print(intValue) // Output: Optional(42)
     */
    
    // MARK: TO Double
    var toDouble: Double? {
        return Double(self)
    }

    /*
     // Example usage:
     let stringValue = "42.5"
     let doubleValue = stringValue.toDouble
     print(doubleValue) // Output: Optional(42.5)
     */
    
    // MARK: Check if String contains only digits
    var isNumeric: Bool {
        return !isEmpty && range(of: "[^0-9]", options: .regularExpression) == nil
    }

    /*
     // Example usage:
     let stringValue = "12345"
     let isNumeric = stringValue.isNumeric
     print(isNumeric) // Output: true
     */
    
    // MARK: String trimming whitespace and newlines
    var trimmed: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    /*
     // Example usage:
     let stringValue = "  Hello, world! \n"
     let trimmedValue = stringValue.trimmed
     print(trimmedValue) // Output: "Hello, world!"
     */
    
    // MARK: Removing a substring from a String
    func removing(_ substring: String, caseSensitive: Bool = true) -> String {
        if caseSensitive {
            return self.replacingOccurrences(of: substring, with: "")
        } else {
            let options: NSString.CompareOptions = [.caseInsensitive]
            return self.replacingOccurrences(of: substring, with: "", options: options)
        }
    }

    /*
     // Example usage:
     let stringValue = "Hello, world!"
     let withoutWorld = stringValue.removing("world", caseSensitive: false)
     print(withoutWorld) // Output: "Hello, !"
     */

    // MARK: Checking if a String contains a given substring
    func contains(_ substring: String, caseSensitive: Bool = true) -> Bool {
        if caseSensitive {
            return self.contains(substring)
        } else {
            return self.lowercased().contains(substring.lowercased())
        }
    }

    /*
     // Example usage:
     let stringValue = "Hello, world!"
     let containsWorld = stringValue.contains("world", caseSensitive: false)
     print(containsWorld) // Output: true
     */
    
    // MARK: Capitalizing the first letter of a String
    var capitalizedFirst: String {
        guard !isEmpty else { return self }
        return prefix(1).capitalized + dropFirst()
    }

    /*
     // Example usage:
     let stringValue = "hello, world!"
     let capitalizedValue = stringValue.capitalizedFirst
     print(capitalizedValue) // Output: "Hello, world!"
     */
}
