//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

public enum DefaultDataType {
    case bool
    case int
    case double
    case string
    case custom(Codable.Type)
}

public extension UserDefaults {
    func set(_ value: Any, type: DefaultDataType, forKey key: String) {
        switch type {
        case .bool:
            set(value as? Bool, forKey: key)
        case .int:
            set(value as? Int, forKey: key)
        case .double:
            set(value as? Double, forKey: key)
        case .string:
            set(value as? String, forKey: key)
        case .custom(_):
            guard let value = value as? Codable, let encodedValue = try? JSONEncoder().encode(value) else { return }
            set(encodedValue, forKey: key)
        }
    }

    func get(type: DefaultDataType, forKey key: String) -> Any? {
        switch type {
        case .bool:
            return bool(forKey: key)
        case .int:
            return integer(forKey: key)
        case .double:
            return double(forKey: key)
        case .string:
            return string(forKey: key)
        case .custom(let type):
            guard let data = data(forKey: key), let value = try? JSONDecoder().decode(type.self, from: data) else { return nil }
            return value
        }
    }
    
    /*
     // Custom Codable type
     struct CustomType: Codable {
         let name: String
         let age: Int
     }

     // Saving values
     let customValue = CustomType(name: "John Doe", age: 42)
     UserDefaults.standard.set(customValue, type: .custom(CustomType.self), forKey: "customValueKey")

     // Retrieving values
     if let retrievedValue = UserDefaults.standard.get(type: .custom(CustomType.self), forKey: "customValueKey") as? CustomType {
         print(retrievedValue) // Output: CustomType(name: "John Doe", age: 42)
     }

     */
}
