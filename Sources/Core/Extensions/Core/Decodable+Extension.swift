//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

public extension Decodable {
    // MARK: From Dictionary
    init?(fromDictionary dictionary: [String: Any]) {
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: []) else { return nil }
        guard let object = try? JSONDecoder().decode(Self.self, from: data) else { return nil }
        self = object
    }
    
    /*
     
     // Example usage:
     let dictionary: [String: Any] = ["id": 1, "name": "John Doe", "email": "john.doe@example.com"]
     let user = User(fromDictionary: dictionary)

     */
}
