//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//

import SwiftUI

public struct DIKeyAppState: EnvironmentKey {
    public typealias Value = AppState
    public static var defaultValue: AppState = AppState()
}

public extension EnvironmentValues {
    
    var appState: AppState {
        get {
            return self[DIKeyAppState.self]
        }
        set {
            self[DIKeyAppState.self] = newValue
        }
    }
}
