//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation
import SwiftUI

public extension View {
    // MARK: Applying a custom font modifier
    func customFont(_ name: String, size: CGFloat) -> some View {
        self.modifier(CustomFontModifier(name: name, size: size))
    }
    
    /*
     // Example usage:
     Text("Hello, SwiftUI!")
         .customFont("HelveticaNeue-Bold", size: 24)
     */
    
    //MARK: Adding a background blur effect
    func backgroundBlur(radius: CGFloat) -> some View {
        self.modifier(BackgroundBlurModifier(radius: radius))
    }

    /*
     // Example usage:
     Text("Hello, SwiftUI!")
         .backgroundBlur(radius: 10)
     */
    
    // MARK: Adding a custom border
    func customBorder<S>(_ content: S, width: CGFloat) -> some View where S: ShapeStyle {
        self.modifier(CustomBorderModifier(content: content, width: width))
    }

    /*
     // Example usage:
     Text("Hello, SwiftUI!")
         .padding()
         .customBorder(Color.red, width: 2)
     */
    
    // MARK: Single Side Shadow
    func singleSideShadow(color: Color = .black, radius: CGFloat = 10, x: CGFloat = 0, y: CGFloat = 0) -> some View {
        self.modifier(SingleSideShadow(color: color, radius: radius, x: x, y: y))
    }

    /*
     // Example usage:
     Text("Hello, SwiftUI!")
         .singleSideShadow(color: .black, radius: 5, x: 5, y: 5)
     */
    
    // MARK: Rotate View
    func rotate(degrees: Double) -> some View {
        self.modifier(RotateModifier(degrees: degrees))
    }

    /*
     // Example usage:
     Text("Hello, SwiftUI!")
         .rotate(degrees: 45)
     */
    
    // MARK: Coditional Modifier
    @ViewBuilder
    func `if`<Content: View>(_ condition: Bool, apply: (Self) -> Content) -> some View {
        if condition {
            apply(self)
        } else {
            self
        }
    }

    /*
     // Example usage:
     struct ContentView: View {
         @State private var isHighlighted = false

         var body: some View {
             VStack {
                 Button("Toggle Highlight") {
                     isHighlighted.toggle()
                 }

                 Text("Hello, SwiftUI!")
                     .if(isHighlighted) {
                         $0.background(Color.yellow)
                             .padding()
                     }
             }
         }
     }
     */
    
    // MARK: Watermark
    func watermark(_ text: String) -> some View {
        self.modifier(WatermarkModifier(text: text))
    }
    
    /*
     // Example usage:
     Image("example-image")
         .resizable()
         .scaledToFit()
         .watermark("Copyright 2023")
     */
    
    // MARK: Adding a keyboard dismissal button
    func dismissKeyboardButton() -> some View {
        self.modifier(DismissKeyboardButtonModifier())
    }
    
    /*
     // Example usage:
     NavigationView {
         TextField("Enter some text", text: $inputText)
             .padding()
             .dismissKeyboardButton()
     }
     */

    // MARK: f
    func customCornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        self.modifier(CustomCornerRadiusModifier(radius: radius, corners: corners))
    }

    /*
     // Example usage:
     Text("Hello, SwiftUI!")
         .padding()
         .background(Color.green)
         .customCornerRadius(10, corners: [.topLeft, .bottomRight])
     */
    
    // MARK: Applying a gradient background
    func gradientBackground(colors: [Color]) -> some View {
        self.modifier(GradientBackgroundModifier(colors: colors))
    }

    /*
    // Example usage:
    Text("Hello, SwiftUI!")
        .gradientBackground(colors: [.red, .orange])
     */
    
    // MARK: SHAKE
    func shake(times: Int, offset: CGFloat) -> some View {
            self.modifier(ShakeModifier(times: times, offset: offset))
        }

    /*
     // Example usage:
     Text("Hello, SwiftUI!")
         .shake(times: 5, offset: 10)
     */
    
    //MARK: Applying a circular reveal effect
    func circularReveal(isRevealed: Bool) -> some View {
        self.modifier(CircularRevealModifier(isRevealed: isRevealed))
    }

    /*
     // Example usage:
     struct ContentView: View {
         @State private var isRevealed = false

         var body: some View {
             VStack {
                 Button("Toggle Reveal") {
                     isRevealed.toggle()
                 }

                 Text("Hello, SwiftUI!")
                     .font(.largeTitle)
                     .padding()
                     .background(Color.blue)
                     .foregroundColor(.white)
                     .circularReveal(isRevealed: isRevealed)
             }
         }
     }
     */
    
    // MARK: Rotate
    func pulsate(scale: CGFloat, duration: TimeInterval) -> some View {
        self.modifier(PulsateModifier(scale: scale, duration: duration))
    }

    /*
     // Example usage:
     Circle()
         .frame(width: 100, height: 100)
         .foregroundColor(.red)
         .pulsate(scale: 1.2, duration: 1)
     */
    
    // MARK: Custom InOut Animation
    func customAnimation(_ effect: AnimationEffect, duration: TimeInterval) -> some View {
        self.modifier(CustomAnimationModifier(effect: effect, duration: duration))
    }

    /*
     // Example usage:
     Text("Hello, SwiftUI!")
         .font(.largeTitle)
         .customAnimation(.fadeInOut, duration: 1)

     Text("Hello, SwiftUI!")
         .font(.largeTitle)
         .customAnimation(.scaleEffect(1.2), duration: 1)

     Text("Hello, SwiftUI!")
         .font(.largeTitle)
         .customAnimation(.rotateEffect(45), duration: 1)
     */
}
