//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

public extension Int {
    // MARK: TO Double
    var toDouble: Double {
        return Double(self)
    }
    
    /*
     // Example usage:
     let intValue = 42
     let doubleValue = intValue.toDouble
     print(doubleValue) // Output: 42.0
     */
    
    // MARK: TO Double
    var toString: String {
        return String(self)
    }

    /*
    // Example usage:
    let intValue = 42
    let stringValue = intValue.toString
    print(stringValue) // Output: "42"
    */
    
    // MARK: IS Even?
    var isEven: Bool {
        return self % 2 == 0
    }

    /*
     // Example usage:
     let intValue = 42
     let isEven = intValue.isEven
     print(isEven) // Output: true
    */
    
    // MARK: IS Odd?
    var isOdd: Bool {
        return self % 2 != 0
    }

    /*
     // Example usage:
     // Example usage:
     let intValue = 42
     let isOdd = intValue.isOdd
     print(isOdd) // Output: false
     */
    
    // MARK: Int with ordinal suffix
    var ordinal: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .ordinal
        return formatter.string(from: NSNumber(value: self)) ?? ""
    }

    /*
     // Example usage:
     let intValue = 42
     let ordinalValue = intValue.ordinal
     print(ordinalValue) // Output: "42nd"
    */
}
