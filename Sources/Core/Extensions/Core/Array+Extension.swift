//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

public extension Array {
    
    // MARK: Array filtering
    func filter(_ condition: (Element) -> Bool) -> [Element] {
        return self.compactMap { condition($0) ? $0 : nil }
    }
    
    /*
     // Example usage:
     let numbers = [1, 2, 3, 4, 5]
     let evenNumbers = numbers.filter { $0 % 2 == 0 }
     print(evenNumbers) // Output: [2, 4]
    */
    
    // MARK: Array mapping
    func map<T>(_ transform: (Element) -> T) -> [T] {
            return self.compactMap { transform($0) }
        }

    /*
     // Example usage:
     let numbers = [1, 2, 3, 4, 5]
     let squaredNumbers = numbers.map { $0 * $0 }
     print(squaredNumbers) // Output: [1, 4, 9, 16, 25]
    */
    
    // MARK: Array grouping
    func groupBy<Key: Hashable>(_ keyForValue: (Element) -> Key) -> [Key: [Element]] {
        var grouped = [Key: [Element]]()
        for element in self {
            let key = keyForValue(element)
            if var group = grouped[key] {
                group.append(element)
                grouped[key] = group
            } else {
                grouped[key] = [element]
            }
        }
        return grouped
    }

    /*
     // Example usage:
     let names = ["Alice", "Bob", "Charlie", "David", "Eve"]
     let groupedNames = names.groupBy { $0.first! }
     print(groupedNames) // Output: ["A": ["Alice"], "B": ["Bob"], "C": ["Charlie"], "D": ["David"], "E": ["Eve"]]
    */
        
    // MARK: Array element at index safely
    func element(at index: Int) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }

    /*
     // Example usage:
     let numbers = [1, 2, 3, 4, 5]
     print(numbers.element(at: 2)) // Output: Optional(3)
     print(numbers.element(at: 10)) // Output: nil
    */
    
    // MARK: Array finding first element matching a condition
    func first(where condition: (Element) -> Bool) -> Element? {
        return self.first(where: condition)
    }

    /*
     // Example usage:
     let numbers = [1, 2, 3, 4, 5]
     let firstEvenNumber = numbers.first { $0 % 2 == 0 }
     print(firstEvenNumber) // Output: Optional(2)
    */
    
    // MARK: Array contains where
    func contains(where condition: (Element) -> Bool) -> Bool {
        return self.contains(where: condition)
    }

    /*
     // Example usage:
     let numbers = [1, 2, 3, 4, 5]
     let hasEvenNumber = numbers.contains { $0 % 2 == 0 }
     print(hasEvenNumber) // Output: true
    */
    
    // MARK: Array partition by predicate
    func partition(by condition: (Element) -> Bool) -> ([Element], [Element]) {
        return self.reduce(into: ([], [])) { result, element in
            if condition(element) {
                result.0.append(element)
            } else {
                result.1.append(element)
            }
        }
    }

    /*
     // Example usage:
     let numbers = [1, 2, 3, 4, 5]
     let (evenNumbers, oddNumbers) = numbers.partition(by: { $0 % 2 == 0 })
     print(evenNumbers) // Output: [2, 4]
     print(oddNumbers) // Output: [1, 3, 5]
    */
    
    // MARK: Array chunks of fixed size
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }

    /*
     // Example usage:
     let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
     let chunkedNumbers = numbers.chunked(into: 3)
     print(chunkedNumbers) // Output: [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    */
}


extension Array where Element: Hashable {
     
    // MARK: Array removing duplicates
    func removeDuplicates() -> [Element] {
        var seen = Set<Element>()
        return self.filter { seen.insert($0).inserted }
    }
    
    /*
     // Example usage:
     let arrayWithDuplicates = [1, 2, 3, 2, 1, 4, 5, 4, 6]
     let uniqueArray = arrayWithDuplicates.removeDuplicates()
     print(uniqueArray) // Output: [1, 2, 3, 4, 5, 6]
     */
}


extension Array where Element: Codable {
    
    // MARK: Mapping an array of Codable objects to a specific property
    func map<T>(_ keyPath: KeyPath<Element, T>) -> [T] {
        return self.map { $0[keyPath: keyPath] }
    }
    
    /*
     // Example usage:
     let users = [
         User(id: 1, name: "John Doe", email: "john.doe@example.com"),
         User(id: 2, name: "Jane Doe", email: "jane.doe@example.com")
     ]

     let names = users.map(\.name) // ["John Doe", "Jane Doe"]
     */
    
    // MARK: Filter by property and condition
    func filter<T: Equatable>(_ keyPath: KeyPath<Element, T>, equalTo value: T) -> [Element] {
        return self.filter { $0[keyPath: keyPath] == value }
    }

    /*
     // Example usage:
     let users = [
         User(id: 1, name: "John Doe", email: "john.doe@example.com"),
         User(id: 2, name: "Jane Doe", email: "jane.doe@example.com"),
         User(id: 3, name: "John Smith", email: "john.smith@example.com")
     ]

     let filteredUsers = users.filter(\.name, equalTo: "John Doe") // [User(id: 1, name: "John Doe", email: "john.doe@example.com")]

     */
    
    // MARK: Sorting an array of Codable objects based on a specific property
    func sorted<T: Comparable>(by keyPath: KeyPath<Element, T>, ascending: Bool = true) -> [Element] {
        return self.sorted { a, b in
            return ascending ? a[keyPath: keyPath] < b[keyPath: keyPath] : a[keyPath: keyPath] > b[keyPath: keyPath]
        }
    }
    
    /*
     // Example usage:
     let users = [
         User(id: 2, name: "Jane Doe", email: "jane.doe@example.com"),
         User(id: 1, name: "John Doe", email: "john.doe@example.com"),
         User(id: 3, name: "John Smith", email: "john.smith@example.com")
     ]

     let sortedUsers = users.sorted(by: \.id) // Sorted by id in ascending order
     */

}
