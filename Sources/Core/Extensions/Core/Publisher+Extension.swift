//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Combine
import Foundation

public extension URLSession.DataTaskPublisher {
    func sinkDecodeToResult<T: Decodable>(_ type: T.Type, using decoder: JSONDecoder = JSONDecoder(), receiveCompletion: @escaping (Subscribers.Completion<Error>) -> Void = { _ in }, receiveValue: @escaping (Result<T, Error>) -> Void) -> AnyCancellable {
        return self.handleNetworkResponse()
            .decode(type: T.self, decoder: decoder)
            .mapError { error -> Error in
                if let urlError = error as? URLError {
                    return urlError
                } else {
                    return error
                }
            }
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    receiveValue(.failure(error))
                case .finished:
                    break
                }
            }, receiveValue: { customStruct in
                receiveValue(.success(customStruct))
            })
    }
}

public extension Publisher where Output == URLSession.DataTaskPublisher.Output {
    func handleNetworkResponse() -> AnyPublisher<Data, Error> {
        return self.tryMap { data, response -> Data in
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                throw URLError(.badServerResponse)
            }
            return data
        }
        .mapError { error -> Error in
            if let urlError = error as? URLError {
                return urlError
            } else {
                return error
            }
        }
        .eraseToAnyPublisher()
    }
}
