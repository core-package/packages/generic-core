//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

public extension Date {
    // MARK: Date format
    func format(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    /*
     // Example usage:
     let date = Date()
     print(date.format("yyyy-MM-dd")) // Output: "2023-04-18" (or current date)
     */
    
    // MARK: Adding time components
    func adding(_ component: Calendar.Component, value: Int) -> Date {
        return Calendar.current.date(byAdding: component, value: value, to: self) ?? self
    }
    
    /*
     // Example usage:
     let date = Date()
     let oneWeekLater = date.adding(.day, value: 7)
     print(oneWeekLater) // Output: Date representing one week after the current date
     */
    
    
    // MARK: Days between dates
    func daysBetween(_ date: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: self, to: date)
        return components.day ?? 0
    }

    /*
     // Example usage:
     let date1 = Date()
     let date2 = date1.adding(.day, value: 7)
     let daysBetween = date1.daysBetween(date2)
     print(daysBetween) // Output: 7
     */
    
    
    // MARK: Start of the day
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }

    /*
     // Example usage:
     let date = Date()
     let startOfDay = date.startOfDay
     print(startOfDay) // Output: Date representing the start of the current day (at 00:00:00)
     */
    
    
    // MARK: End of the day
    var endOfDay: Date? {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)
    }

    /*
     // Example usage:
     let date = Date()
     let endOfDay = date.endOfDay
     print(endOfDay) // Output: Date representing the end of the current day (at 23:59:59)
     */
}
