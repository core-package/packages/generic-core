//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation
import SwiftUI
import SDWebImageSwiftUI

public extension Image {
    
    // MARK: Fetch Image
    static func fromUrl(_ url: String, placeholder: Image? = nil) -> some View {
        Group {
            if let url = URL(string: url) {
                WebImage(url: url)
                    .resizable()
                    .placeholder {
                        placeholder
                    }
            } else {
                placeholder
            }
        }
    }
    
    /*
     // Example usage:
     Image.fromUrl("https://example.com/image.jpg", placeholder: Image(systemName: "photo"))
     .aspectRatio(contentMode: .fit)
     */
    
    // MARK: Grayscale
    func grayscale() -> some View {
        self.modifier(GrayscaleModifier())
    }
    
    /*
     // Example usage:
     Image("example-image")
     .resizable()
     .grayscale()
     */
    
    // MARK: Custom Shape
    func customShape<S: Shape>(_ shape: S) -> some View {
        self.modifier(CustomShapeModifier(shape: shape))
    }

    /*
     // Example usage:
     Image("example-image")
         .resizable()
         .scaledToFit()
         .customShape(Circle())
     */
}
