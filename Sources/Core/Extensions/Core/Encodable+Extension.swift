//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

public extension Encodable {
    // MARK: TO Dictionary
    func toDictionary() -> [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
    
    /*
     // Example usage:
     struct User: Codable {
         var id: Int
         var name: String
         var email: String
     }

     let user = User(id: 1, name: "John Doe", email: "john.doe@example.com")
     let userDict = user.toDictionary()
     */
    
    // MARK: TO JSON
    func toJSONString() -> String? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
    /*
     // Example usage:
     let jsonString = user.toJSONString()
     */
    
    // MARK: Pretty Print Log
    func prettyPrinted() {
        guard let data = try? JSONEncoder().encode(self),
              let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []),
              let jsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted),
              let jsonString = String(data: jsonData, encoding: .utf8) else { return }
        print(jsonString)
    }

    /*
     // Example usage:
     user.prettyPrinted()
     */

}
