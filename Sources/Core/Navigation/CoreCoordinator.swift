//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import FlowStacks
import SwiftUI

public protocol Coordinator {
    var routes: Routes<any View> { get } // On Init - self.routes = [.root(.home(.init(View)))]
    var interactor: Interactor { get }
    func goBack() // routes.goBack()
    func goBackToRoot() // routes.goBackToRoot()
}
/*  USAGE IN
    struct AppCoordinator: View {
    @ObservedObject var viewModel: AppCoordinatorViewModel
        
    var body: some View {
        Router($viewModel.routes) { screen in
        switch screen {
        case .home(let viewModel):
            HomeView(viewModel: viewModel)
        case .numberList(let viewModel):
            NumberListView(viewModel: viewModel)
        case .number(let viewModel):
            NumberView(viewModel: viewModel)
        }
        }
    }
 
    }
*/
