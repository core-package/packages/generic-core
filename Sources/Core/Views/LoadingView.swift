//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  
import SwiftUI

@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
public struct LoadingView<Content>: View where Content: View {

    @Environment(\.appState) var appState: AppState

    public var content: Content
    public var title: String?

    @Binding public var isLoading: Bool

    public init(isLoading: Binding<Bool>, title: String?, @ViewBuilder content: () -> Content) {
        self.content = content()
        self.title = title
        self._isLoading = isLoading
    }

    @MainActor public var body: some View {
        VStack {
            if isLoading {
                ZStack {
                    VStack(alignment: .center, spacing: 10) {
                        LoadingIndicator(animation: .threeBallsBouncing, color: .white, size: .medium, speed: .normal)
                        
                        Text(title ?? "Loading...")
                            .font(Font(appState.theme.typography.inputDescription))
                            .padding(.top, 20)
                            .multilineTextAlignment(.center)
                    }                    
                }
                .frame(width: w(), height: h())
                .background(appState.theme.colors.background)
                .foregroundColor(appState.theme.colors.foreground)
                
            } else {
                ZStack {
                    ScrollView {
                        content
                    }
                    .padding(.top, 60)
                    .padding(.bottom, 100)
                }
                .frame(width: w(), height: h())
                .background(appState.theme.colors.background)
                .foregroundColor(appState.theme.colors.foreground)
            }
        }
    }
    
    public init(@ViewBuilder content: () -> Content) {
        self.content = content()
        self._isLoading = Binding.constant(false)
    }
}


func w() -> CGFloat {
    return UIScreen.main.bounds.size.width
}

func h() -> CGFloat {
    return UIScreen.main.bounds.size.height
}
