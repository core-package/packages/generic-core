//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//

import SwiftUI

struct CountdownView: View {
    
    @Environment(\.appState) var appState: AppState
    
    let targetDate: Date
    @State private var remainingTime: TimeInterval = 0.0
    
    var body: some View {
        Text(timeString(time: remainingTime))
            .font(Font(appState.theme.typography.h4))
            .onAppear {
                var timer: Timer? = nil
                remainingTime = targetDate.timeIntervalSince(Date())
                timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
                    remainingTime = max(targetDate.timeIntervalSince(Date()), 0.0)
                    if remainingTime == 0.0 {
                        timer?.invalidate()
                        timer = nil
                        // Perform action when time is up
                    }
                }
            }
    }
    
    private func timeString(time: TimeInterval) -> String {
        let days = Int(time) / (3600 * 24)
        let hours = Int(time) / 3600 % 24
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format: "%02d:%02d:%02d:%02d", days, hours, minutes, seconds)
    }
}

struct CountdownView_Previews: PreviewProvider {
    static var previews: some View {
        CountdownView(targetDate: Date().addingTimeInterval(3600))
    }
}
