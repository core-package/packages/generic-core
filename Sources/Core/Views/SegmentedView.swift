//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

struct SegmentedItem {
    let title: String
    let view: AnyView
    
    init<Content: View>(title: String, @ViewBuilder view: () -> Content) {
        self.title = title
        self.view = AnyView(view())
    }
}

struct SegmentedControlView: View {
    
    @Environment(\.appState) var appState: AppState

    let items: [SegmentedItem]
    @State private var selection = 0
    
    var body: some View {
        VStack(spacing: 3) {
            seperator
            menu
            seperator
            VStack {
                items[selection].view
                    .frame(minWidth: w())
            }
        }
    }
    
    var seperator: some View {
        return VStack {
            Rectangle()
                .foregroundColor(.white)
                .frame(height: 1.2)
        }
    }
    
    var menu: some View {
        HStack(spacing: 0) {
            ForEach(0..<items.count) { index in
                Text(items[index].title)
                    .textCase(.uppercase)
                    .font(Font(appState.theme.typography.caption))
                    .foregroundColor(selection == index ?
                                     appState.theme.colors.primary : appState.theme.colors.foreground)
                    .frame(maxWidth: 100)
                    .onTapGesture { selection = index }
            }
        }
        .padding(.vertical, 8)
    }
}

struct SegmentedControlView_Previews: PreviewProvider {
    static var previews: some View {
        let items = [
            SegmentedItem(title: "Compare") {
                Text("Content Compare").foregroundColor(.white)
            },
            
            SegmentedItem(title: "Stats") {
                Text("Content Stats").foregroundColor(.white)
            },
            
            SegmentedItem(title: "Highlights") {
                Text("Content Highlights").foregroundColor(.white)
            },
            
            SegmentedItem(title: "Props") {
                Text("Content Props").foregroundColor(.white)
            }
        ]
        VStack {
            SegmentedControlView(items: items)
        }
        .padding(.vertical)
        .background(Color.black)
    }
}
