//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

struct CTAButton: View {
    @Environment(\.appState) var appState: AppState

    let image: String
    let title: String
    var positive: Bool?
    let action: () -> Void
    
    var color: Color {
        positive ?? true ? appState.theme.colors.primary:
        appState.theme.colors.secondary
    }
    var body: some View {
        Button(action: action) {
            HStack(spacing: 10) {
                Image(systemName: image)
                    .font(.system(size: 20))
                
                Text(title)
                    .font(Font(appState.theme.typography.button))
                    .foregroundColor(appState.theme.colors.primary)
                    .fontWeight(.bold)
            }
            .padding(10)
            .padding(.horizontal, 15)
            .frame(minWidth: w() - 30, maxHeight: 40)
            .background(color)
            .foregroundColor(appState.theme.colors.background)
            .cornerRadius(8)
        }
    }
}

