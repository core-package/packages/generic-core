//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  
import SwiftUI

struct DialogView: View {
    @Environment(\.appState) var appState: AppState
    @Binding var result: Bool
    let title: String
    let button1Title: String
    let button2Title: String
    let button1Action: ()
    let button2Action: ()
    
    var body: some View {
        VStack(spacing: 10) {
            titleText
                .font(.headline)
                .padding(.bottom, 25)
                .multilineTextAlignment(.center)
                .padding(.horizontal, 20)
            
            VStack(spacing: 15) {
                CTAButton(image: "",
                          title: button1Title,
                          positive: true) {
                    button1Action
                }
                CTAButton(image: "",
                          title: button2Title,
                          positive: false) {
                    button2Action
                }
            }
            .padding(.bottom, 16)
        }
        .padding(.vertical, 30)
        .padding(.horizontal, 20)
        .frame(minWidth: 300)
        .background(appState.theme.colors.background)
        .cornerRadius(16)
        .shadow(radius: 8)
    }
    
    var titleText: some View {
        Text(title)
            .font(Font(appState.theme.typography.button))
            .foregroundColor(appState.theme.colors.foreground)
            .frame(minWidth: 250, minHeight: 45)
    }
    
    var button1Text: some View {
        Text(button2Title)
            .font(Font(appState.theme.typography.button))
            .foregroundColor(appState.theme.colors.foreground)
            .background(appState.theme.colors.primary)
            .frame(minWidth: 250, minHeight: 45)
    }

    var button2Text: some View {
        Text(button1Title)
            .font(Font(appState.theme.typography.button))
            .foregroundColor(appState.theme.colors.foreground)
            .background(appState.theme.colors.secondary)
            .frame(minWidth: 250, minHeight: 45)
    }

}

