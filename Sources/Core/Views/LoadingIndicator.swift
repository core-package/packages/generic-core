//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//

import SwiftUI

public struct LoadingIndicator: View {
    
    let animation: LoadingAnimation
    let size: CGFloat
    let speed: Double
    let color: Color
    
    public init(
        animation: LoadingAnimation = .threeBallsBouncing,
        color: Color = .primary,
        size: Size = .medium,
        speed: Speed = .normal) {
            self.animation = animation
            self.size = size.rawValue
            self.speed = speed.rawValue
            self.color = color
    }
    
    public var body: some View {
        switch animation {
        case .threeBallsBouncing: LoadingThreeBallsBouncing(color: color, size: size, speed: speed)
        }
    }
    
    public enum LoadingAnimation: String, CaseIterable {
        case threeBallsBouncing
    }
    
    public enum Speed: Double, CaseIterable {
        case slow = 1.0
        case normal = 0.5
        case fast = 0.25
        
        var stringRepresentation: String {
            switch self {
            case .slow: return ".slow"
            case .normal: return ".normal"
            case .fast: return ".fast"
            }
        }
    }

    public enum Size: CGFloat, CaseIterable {
        case small = 25
        case medium = 50
        case large = 100
        case extraLarge = 150
        
        var stringRepresentation: String {
            switch self {
            case .small: return ".small"
            case .medium: return ".medium"
            case .large: return ".large"
            case .extraLarge: return ".extraLarge"
            }
        }
    }
}
