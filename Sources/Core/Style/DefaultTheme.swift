//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI
import UIKit

public struct DefaultTheme: Theme {
    public var colors: AppColors
    public var typography: AppTypography
    public var paddings: AppPadding
    
    public init() {
        colors = DefaultAppColors()
        typography = DefaultAppTypography()
        paddings = DefaultAppPadding()
    }
}

public struct DefaultAppColors: AppColors {
    public var primary: Color = Color("#536DFE")
    public var secondary: Color = Color("#FF5C93")
    public var danger: Color = Color("#FF4D4F")
    public var success: Color = Color("#1DBF73")
    public var warning: Color = Color("#FF9800")
    public var info: Color = Color("#4A90E2")
    public var background: Color = Color("#F6F8FB")
    public var foreground: Color = Color("#263238")
    public var shadow: Color = Color("rgba(0, 0, 0, 0.2)")
}

public struct DefaultAppPadding: AppPadding {
    public var screen: CGFloat = 16.0
    public var rows: CGFloat = 8.0
    public var section: CGFloat = 24.0
}

public struct DefaultAppTypography: AppTypography {
    public var h1: UIFont = UIFont.systemFont(ofSize: 96.0, weight: .light)
    public var h2: UIFont = UIFont.systemFont(ofSize: 60.0, weight: .light)
    public var h3: UIFont = UIFont.systemFont(ofSize: 48.0, weight: .regular)
    public var h4: UIFont = UIFont.systemFont(ofSize: 34.0, weight: .regular)
    public var h5: UIFont = UIFont.systemFont(ofSize: 24.0, weight: .regular)
    public var caption: UIFont = UIFont.systemFont(ofSize: 12.0, weight: .regular)
    public var p1: UIFont = UIFont.systemFont(ofSize: 16.0, weight: .regular)
    public var p2: UIFont = UIFont.systemFont(ofSize: 14.0, weight: .regular)
    public var button: UIFont = UIFont.systemFont(ofSize: 14.0, weight: .medium)
    public var placeholder: UIFont = UIFont.systemFont(ofSize: 16.0, weight: .light)
    public var inputDescription: UIFont = UIFont.systemFont(ofSize: 12.0, weight: .light)
    public var inputText: UIFont = UIFont.systemFont(ofSize: 16.0, weight: .regular)
    public var badgeText: UIFont = UIFont.systemFont(ofSize: 12.0, weight: .medium)
}
