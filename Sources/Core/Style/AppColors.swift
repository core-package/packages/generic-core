//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation
import SwiftUI

public protocol AppColors {
    var primary: Color { get }
    var secondary: Color { get }
    var danger: Color { get }
    var success: Color { get }
    var warning: Color { get }
    var info: Color { get }
    var background: Color { get }
    var foreground: Color { get }
    var shadow: Color { get }
}
