//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//


import Foundation
import SwiftUI

public protocol Theme {
    var colors: AppColors { get }
    var typography: AppTypography { get }
    var paddings: AppPadding { get }
}
