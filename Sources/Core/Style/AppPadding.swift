//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

public protocol AppPadding {
    var screen: CGFloat { get }
    var rows: CGFloat { get }
    var section: CGFloat { get }
}
