//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import SwiftUI

public protocol AppTypography {
    var h1: UIFont { get }
    var h2: UIFont { get }
    var h3: UIFont { get }
    var h4: UIFont { get }
    var h5: UIFont { get }
    var caption: UIFont { get }
    var p1: UIFont { get }
    var p2: UIFont { get }
    var button: UIFont { get }
    var placeholder: UIFont { get }
    var inputDescription: UIFont { get }
    var inputText: UIFont { get }
    var badgeText: UIFont { get }
}
